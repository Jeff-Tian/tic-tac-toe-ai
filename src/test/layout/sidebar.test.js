import React from "react";
import {render} from "@testing-library/react";
import {SideBar} from "../../layout/SideBar";
import {BrowserRouter} from "react-router-dom";

test("SideBar snapshot", () => {
    const {asFragment} = render(<BrowserRouter basename={"/"}><SideBar/></BrowserRouter>);
    expect(asFragment()).toMatchSnapshot();
});
