import React from 'react';
import Game from '../../Game/Game';
import {StrategySettings} from "../../Game/Strategy";

import {render, fireEvent, waitFor} from '@testing-library/react';

jest.mock('tone', () => ({
    // Provide necessary mock implementations or return an empty object
}));

test('Game can train players', async () => {
    StrategySettings.setInitialWeights([0, 1, 1])
    StrategySettings.setNamedStrategy((factors) => {
        return {
            const: factors[0],
            danger: factors[1],
            occupyCenter: factors[2],
        };
    })

    let game = render(<Game/>);


    // expect(game.state()).toEqual({
    //     "OWeights": [0, 1, 1],
    //     "autoStart": false,
    //     "countDown": 0,
    //     "currentMode": "humanVsComputer",
    //     "history": [{"squareIndex": null, "squares": [null, null, null, null, null, null, null, null, null]}],
    //     "round": 1,
    //     "stepNumber": 0,
    //     "strategy": {"const": 1, "danger": 0, "occupyCenter": 0},
    //     "winnerInfo": null,
    //     "xIsNext": true,
    //     disabled: false
    // });

    const turnsInput = game.getByTestId('turns');
    const startAutoButton = game.getByTestId('start-auto-button');

    fireEvent.change(turnsInput, {target: {value: '1'}});
    fireEvent.click(startAutoButton);

    await waitFor(() => {

    })
});
