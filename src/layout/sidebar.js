import {List} from "antd-mobile";
import React from "react";
import {Link} from "react-router-dom";
import Resources from "../Game/Resources";

const menuItems = [
    {to: "/", text: Resources.getCurrentCulture().homepage},
    {to: "/about", text: Resources.getCurrentCulture().about},
    {to: "/video", text: Resources.getCurrentCulture().video},
    {to: "/settings", text: Resources.getCurrentCulture().settings},
    {
        to: "https://view.officeapps.live.com/op/view.aspx?src=https%3A%2F%2Ftictactoe.js.org%2F%E5%B0%86%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E5%BA%94%E7%94%A8%E4%BA%8E%E6%A3%8B%E7%B1%BB%E6%B8%B8%E6%88%8F%E5%BC%80%E5%8F%91%E4%B8%AD%E7%9A%84%E4%B8%80%E8%88%AC%E6%AD%A5%E9%AA%A4.docx",
        text: Resources.getCurrentCulture().how
    },
    {
        to: "https://github.com/Jeff-Tian/tic-tac-toe-ai",
        text: Resources.getCurrentCulture().source,
        thumb: (
            <svg
                height="32"
                className="octicon octicon-mark-github"
                viewBox="0 0 16 16"
                version="1.1"
                width="32"
                aria-hidden="true"
            >
                <path
                    fillRule="evenodd"
                    d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0 0 16 8c0-4.42-3.58-8-8-8z"
                />
            </svg>),
    },
    {
        to: "https://id3.js.org",
        text: "id3",
        thumb: "https://id3.js.org/favicon.ico",
    },
    {
        to: "https://gitmoji.js.org",
        text: "gitmoji",
        thumb: "https://gitmoji.js.org/static/favicon-32x32.png",
    },
    {
        to: "/alipay-red-package",
        text: Resources.getCurrentCulture().getRedPackage,
        thumb: require("../Resources/images/alipay-red-package.png"),
    },
    {
        to: "/support-author",
        text: Resources.getCurrentCulture().supportAuthor,
        thumb: require("../Resources/images/alipay-red-package.png"),
    },
]

let fullLink = {width: "100%", display: "inline-block"};
export const SideBar = ({onClicked}) => (
    <List>
        {
            menuItems.map((item, index) => {
                return (
                    <List.Item
                        key={index}
                        thumb={item.thumb}
                        multipleLine={item.multipleLine ?? true}
                    >
                        {item.to.startsWith("http") ?
                            <a href={item.to} onClick={onClicked} style={fullLink} rel="noopener noreferrer"
                               target="_blank">
                                {item.text}
                            </a>
                            :
                            <Link to={item.to} onClick={onClicked} style={fullLink}>
                                {item.text}
                            </Link>
                        }
                    </List.Item>
                )
            })
        }
    </List>
);
