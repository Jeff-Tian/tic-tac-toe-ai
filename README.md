[English](./README.en.md) | 简体中文

# tic-tac-toe-ai （三子棋版阿尔法狗）

> 能够学习的三子棋程序。

[![Build Status](https://travis-ci.org/Jeff-Tian/tic-tac-toe-ai.svg?branch=master)](https://travis-ci.org/Jeff-Tian/tic-tac-toe-ai)
[![Maintainability](https://api.codeclimate.com/v1/badges/57d198bf961c94ea3b22/maintainability)](https://codeclimate.com/github/Jeff-Tian/tic-tac-toe-ai/maintainability)
[![Test Coverage](https://api.codeclimate.com/v1/badges/57d198bf961c94ea3b22/test_coverage)](https://codeclimate.com/github/Jeff-Tian/tic-tac-toe-ai/test_coverage)
[![Git commit with emojis!](https://img.shields.io/badge/gitmoji-git%20commit%20with%20emojis!-brightgreen.svg)](https://gitmoji.js.org)
[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2FJeff-Tian%2Ftic-tac-toe-ai.svg?type=shield)](https://app.fossa.io/projects/git%2Bgithub.com%2FJeff-Tian%2Ftic-tac-toe-ai?ref=badge_shield)

<a href="https://www.producthunt.com/posts/tic-tac-toe-ai?utm_source=badge-featured&utm_medium=badge&utm_souce=badge-tic-tac-toe-ai" target="_blank"><img src="https://api.producthunt.com/widgets/embed-image/v1/featured.svg?post_id=162916&theme=dark" alt="Tic Tac Toe AI - tictactoe, AI, machine learning | Product Hunt Embed" style="width: 250px; height: 54px;" width="250px" height="54px" /></a>

这是我写的第一个 AI 程序。起源于在学习完 react js 的 [tic-tac-toe 教程](https://reactjs.org/tutorial/tutorial.html)后，我对自己说，为什么不把它升级成一个 AI 版本呢？

[![截图](public/images/screenshot.png)](https://tictactoe.js.org)

## 在线访问：

[https://tictactoe.js.org](https://tictactoe.js.org)

## 谁在玩？

![](./public/images/baojiahao.jpeg)

## 本地运行：

```bash
git clone https://github.com/Jeff-Tian/tic-tac-toe-ai.git
npm install -g create-react-app
npm install
npm start
```

## 测试：

```bash
npm test
npm run coverage
```

## 原理：

本程序应用了一般的机器学习方法，如果你对此原理感兴趣，可以查看[本文](./doc/原理.md)。

## 关于我

[Jeff Tian 在 Stackoverflow](https://stackoverflow.com/users/769900/jeff-tian)

[Jeff Tian 在 Github](https://github.com/Jeff-Tian)

## 贡献指南

1. Fork 这个项目（<https://github.com/Jeff-Tian/tic-tac-toe-ai>）
2. 创建你自己的特性分支（`git checkout -b feature/fooBar`）
3. 提交你的改动（`git commit -am '添加了 fooBar 功能'`）
4. 推送代码到分支（`git push origin feature/fooBar`）
5. 提交新的合并请求


## License
[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2FJeff-Tian%2Ftic-tac-toe-ai.svg?type=large)](https://app.fossa.io/projects/git%2Bgithub.com%2FJeff-Tian%2Ftic-tac-toe-ai?ref=badge_large)
