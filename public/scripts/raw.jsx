import * as React from 'react'
import * as ReactDOM from 'react-dom'
import Game from "../../src/Game/Game";

ReactDOM.render(<Game />, document.getElementById('root'))
